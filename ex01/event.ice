module EventManagementSystem {
    module Commons {
        class DateTime {
            long timestamp;
            idempotent int getYear();
            idempotent int getMonth();
            idempotent int getDay();
            idempotent int getHour();
            idempotent int getMinute();
            idempotent int getSecond();
        };
    }

    module Events {
        struct EventId { long id; };
        struct UserId  { long id; };

        enum EventState { CREATED, STARTED, FINISHED };

        exception NotEnoughMembersException {
            int minimum;
        };

        exception TooMuchMembersException {
            int maximum;
        };

        exception EventNotStartedException {};

        interface EventStateChangeListener {
            void onEventStateChanged(in EventId id, in EventState newState);
        }

        class Event {
            EventId id;
            string name;
            string plannedPlace;
            DateTime plannedStartTime;
            int minimum;
            optional(1) int maximum;

            EventState state = EventState.CREATED:


            idempotent void join(in UserId id);
            idempotent void exit(in UserId id);
            idempotent void subscribe(in EventStateChangeListener* listener);

            idempotent void start() throws NotEnoughMembersException, TooMuchMembersException;
            idempotent void finish() throws EventNotStartedException;

            EventId getId();
            EventState getState();
        };

        interface EventBuilder {
            EventBuilder* withName(in string name);
            EventBuilder* withPlannedPlace(in string place);
            EventBuilder* withStartTime(in DateTime startTime);
            EventBuilder* minimumMembers(in int amount);
            EventBuilder* maximumMembers(in int amount);

            Event* build();
        };

        class EventManager {
            sequence<Event*> events;

            sequence<Event*> findAllNotFinishedEvents();
            Event* findEventById(in EventId id);

            EventBuilder* newEvent();
        };
    };
};



